# -*- coding: utf-8 -*-
#!/usr/bin/python
# @Date : 2022-02-20
# @Author : zhu
# @File : wxReply.py
# @Software: VS
import requests
import time
import re,random

class TuLing:
    '''
    图灵机器人
    '''
    def __init__(self,content):
        try:
            emij_list = [ "/::)","/::B","/::|","/:8-)","/::<","/::$","/::P","/::D","/:,@P","/:,@-D"]
            emij = random.choice(emij_list)
            url = 'http://api.qingyunke.com/api.php?key=free&appid=0&msg={}'.format((content))
            html = requests.get(url,timeout=5)
            self.content = html.json()['content']
        except Exception as e:
            try:
                url = 'https://api.ownthink.com/bot?appid=xiaosi&userid=user&spoken={}'.format((content))
                html = requests.get(url)
                data = html.json().get('data')
                if data.get('type') == 5000:
                    self.content = data.get('info').get('text')+emij
                else:
                    self.content = e+'你学习了吗？就跟我聊天'
            except Exception as e:
                self.content = 'I am sleepy'+emij
    def get_content(self):
        return self.content



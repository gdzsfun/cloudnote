import redis
class PyRedis:
    
    def __init__(self, host='127.0.0.1', port=6379):
        pool = redis.ConnectionPool(host=host, port=port, decode_responses=True)
        self.r = redis.Redis(connection_pool=pool)
        
    def set(self, key, value, ex=None, px=None, nx=False, xx=False):
        """
        ex:过期时间 单位秒
        px:过期时间 单位毫秒
        nx:key不存在才执行set
        xx:key存在才执行
        默认，不存在则创建，存在则修改。
        """
        try:
            return self.r.set(key,value,ex,px,nx,xx)
        except Exception as e:
            return False

    def get(self,key):
        try:
            return self.r.get(key)
        except Exception as e:
            return False
    def delete(self,key):
        try:
            return self.r.delete(key)
        except Exception as e:
            return False
            
if __name__ == '__main__':
    import time
    r = PyRedis()
    print(r.set('name','xiaoming',ex=20))
    print(r.get('name'))
    print(time.sleep(21))
    print(r.get('name'))

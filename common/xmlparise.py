import xml.sax
import  xml.dom.minidom
from xml.dom.minidom import parseString
from xml.etree.ElementTree import Element, tostring
from xml.etree.ElementTree import SubElement,fromstring
from xml.etree.ElementTree import ElementTree
from xml.dom import minidom
from datetime import datetime

#打开xml文档
# dom = xml.dom.minidom.parse(data)

#得到文档元素对象
class WxHandler:
    def __init__(self,data):
        self.doc = parseString(data)
        self.collection = self.doc.documentElement
    
    def get_value_by_name(self,name):
        return self.collection.getElementsByTagName(name)[0].childNodes[0].data

    def get_tree_list(self,rootName,data_list):
        print('kaishi')
        root = Element(rootName)
        tree = self.__itear(root,data_list)
        print(tostring(tree))
        return tostring(tree)
    def __itear(self,root,data_list):
        for k1 in data_list:
            root = SubElement(root,k1.get('name'))
            if k1.get('value'):
                root.text = k1.get('value')
            self.__itear(root,k1.get('children'))
        return ElementTree(root)
    def str_to_xml(self,string):
        return tostring(string)


class WxReply:
    '''
    微信公众号回复消息
    '''
    def __init__(self,toUserName,fromUserName,msgType):
        self.toUserName = toUserName
        self.fromUserName = fromUserName
        self.nowTime = int(datetime.now().timestamp())
        self.msgType = msgType
        self.data = f"""<xml>
            <ToUserName><![CDATA[{self.fromUserName}]]></ToUserName>
            <FromUserName><![CDATA[{self.toUserName}]]></FromUserName>
            <CreateTime>{self.nowTime }</CreateTime>
            <MsgType><![CDATA[{self.msgType}]]></MsgType>"""
    
    def set_text(self,content):
        self.data+=f"""
            <Content><![CDATA[{content}]]></Content>
            </xml>
        """
    
    def set_image(self,image_id):
        self.data+=f"""
            <Image>
                <MediaId><![CDATA[{image_id}]]></MediaId>
            </Image>
            </xml>
        """

    def set_image_text(self,title,desc,picurl,url):
        '''
        设置微信图文消息
        :param title  图文标题
        :param desc(str): 图文描述
        :param picurl(str):图文url
        :param url(str): 链接url
        return:
        '''
        self.data+=f"""
            <ArticleCount>1</ArticleCount>
            <Articles>
                <item>
                <Title><![CDATA[{title}]]></Title>
                <Description><![CDATA[{desc}]]></Description>
                <PicUrl><![CDATA[{picurl}]]></PicUrl>
                <Url><![CDATA[{url}]]></Url>
                </item>
            </Articles>
            </xml>"""

    def get_data(self):
        return self.data




if __name__ == '__main__':
    # 创建一个 XMLReader
    # parser = xml.sax.make_parser()
    # # turn off namepsaces
    # parser.setFeature(xml.sax.handler.feature_namespaces, 0)
    
    data = """<xml>
    <ToUserName><![CDATA[接收者]]></ToUserName>
    <FromUserName><![CDATA[开发者微信号]]]></FromUserName>
    <CreateTime>发送时间</CreateTime>
    <MsgType><![CDATA[文本格式]]]></MsgType>
    <Content><![CDATA[你好]]></Content>
    </xml>"""
    # # 重写 ContextHandler
    Handler = fromstring(data)
    print(dir(Handler))
    # parser.setContentHandler( Handler )
    
    # parser.parse(data)
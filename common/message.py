# -*- coding: utf-8 -*-
#!/usr/bin/python
# @Date : 2022-02-27
# @Author : zhu
# @File : message.py
# @Software: VS
from django.core.mail import send_mail
from django.conf import settings

# qq用户注册成功提示
def qq_register_success(nickname,pwd,ip):
    send_mail(
        'QQ用户注册给点知识网',
        'qq_na:'+nickname+' mi:'+pwd+' ip:'+ip+"用户注册成功",
        settings.ADMIN_EMAIL,
        [settings.ADMIN_EMAIL],
        fail_silently=False,
    )

# 用户注册成功提示
def register_success(username,pwd,ip):
    send_mail(
        '用户注册给点知识网',
        'na:'+username+' mi:'+pwd+' ip:'+ip+"用户注册成功",
        settings.ADMIN_EMAIL,
        [settings.ADMIN_EMAIL],
        fail_silently=False,
    )
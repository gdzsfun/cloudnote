from django.contrib import admin
from .models import User,QQUser
# Register your models here.
class UserAdmin(admin.ModelAdmin):

    list_display = ['id','username','create_time','update_time','age']

    def __str__(self,obj):
        return obj.username

class QQUserAdmin(admin.ModelAdmin):

    list_display = ['id','figureurl_img','nickname','user_id','gender','openid']

    def __str__(self,obj):
        return obj.nickname

admin.site.register(User,UserAdmin)
admin.site.register(QQUser,QQUserAdmin)
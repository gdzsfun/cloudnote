# Generated by Django 2.2 on 2021-09-04 11:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='salt',
            field=models.CharField(default='', max_length=10, verbose_name='盐'),
        ),
    ]

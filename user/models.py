# -*- coding: utf-8 -*-
#!/usr/bin/python
# @Date : 2021-08-28
# @Author : zhu
# @File : models.py
# @Software: VS
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.utils.html import format_html
# Create your models here.

class User(models.Model):
    BIND_QQ_STATUS = (
        (0,'未绑定'),
        (1,'已绑定')
    )
    GENDER=(
        (0,'女'),
        (1,'男'),
        (2,'未知')
    )
    username=models.CharField(verbose_name='用户名', max_length=30,unique=True)
    password=models.CharField(verbose_name='密码', max_length=64)
    salt = models.CharField(verbose_name='盐', max_length=10,default='')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True,null=True,blank=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True,null=True,blank=True)
    level = models.IntegerField(choices=settings.LEVEL,default=0,verbose_name='用户等级',null=True,blank=True)
    mark = models.IntegerField(default=0,verbose_name='用户积分',null=True,blank=True)
    notes = models.CharField(null=True,blank=True,max_length=1024, verbose_name='简介')
    age = models.IntegerField(null=True,blank=True,verbose_name='年龄')
    gender = models.IntegerField(choices=GENDER,default=1,verbose_name='性别',null=True,blank=True)
    is_bind_qq = models.IntegerField(default=0,null=True,blank=True,verbose_name='是否绑定qq')
    figureurl = models.CharField(max_length=256, verbose_name='头像地址', default="images/figureurl/tiger.png")
    
    def __str__(self):
        return 'username %s' % self.username

    class Meta:
        db_table = 'user'
        verbose_name = '点点用户'
        verbose_name_plural = verbose_name



class QQUser(models.Model):
    
    user_id = models.IntegerField(verbose_name='用户id', blank=True, null=True)
    figureurl = models.CharField(max_length=256, verbose_name='头像')
    nickname = models.CharField(max_length=100, verbose_name='昵称')
    gender = models.CharField(max_length=2, default='未知',verbose_name="性别")
    openid = models.CharField(max_length=100,verbose_name="openid")
    
    def figureurl_img(self):
        return format_html(
            '<img src="{}" height="30" width="30" alt="{}" />',
            self.figureurl,
            self.nickname,
        )
    class Meta:
        db_table = 'qq_user'
        verbose_name = 'QQ用户'
        verbose_name_plural = verbose_name
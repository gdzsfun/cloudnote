from django import template
from django.utils.html import format_html
import json

register = template.Library()

@register.filter
def username_to_cn(value):
    try:
        return json.loads(value)
    except:
        return value

@register.filter
def tag_style(value):
    try:
        if value == 1:
            return 'danger'
        elif value == 2:
            return 'warning'
        elif value == 3:
            return 'success'
        else:
            return 'primary'
    except:
        return 'primary'
    
from django.conf.urls import url, re_path
from .views import *
urlpatterns = [
    re_path('^register/$',register_view,name='user_register'),
    re_path('^login/$',login_view,name='user_login'),
    re_path('^qqLogin/$',QQLogin302.as_view(),name='user_qq_login'),
    re_path('^qqLogin_two/$',QQLogin.as_view(),name='user_qq_login'),
]
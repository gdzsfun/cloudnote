# django项目(持续更新中……)Star一下  
==> ` :muscle: ` README持续更新中，更新目标：有点django基础或者计算机基础的一看就懂，达到一个脚本就可以部署的级别==  
==> ` :muscle: ` 如果您有好的想法可以在开放open中提交您的代码==    
==技术交流群1：495421148==  


🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀  
首页演示网站：https://kkndme.xyz/  
网站导航页：https://kkndme.xyz/nav/index/  
后台演示网站：https://kkndme.xyz/admin/  
攻击测试：https://www.kkndme.xyz/nav/index/?aa=../../
测试账号：test  
测试密码：test#test  
🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀

## 1. 项目简介
#### 功能介绍
django 笔记，添加笔记，查看笔记，初始功能
#### 软件架构
软件架构说明
#### 版本介绍
1. mariadb 10.6.8
2. django2.2
3. python3.6
3. redis 5.0.10
#### 安装说明
##### 1.安装前提
1. 找一个服务器，如果是测试可以使用本地虚拟机推荐使用centos7.x测试
2. 使用nginx+uwsgi部署
##### 2. 安装方式
==Docker安装==
1. 创建工作目录
```shell
在/var/www/下创建目录
my_notes # 项目目录
|——git上的项目，也就是本项目拉下开放在这里
mysql # docker mysql映射本地目录
redis # docker redis映射本地目录
d├── conf # 配置文件映射
d└── data # 持久化映射目录
nginx # docker nginx 相关目录[-：普通文件，d:目录文件]
-├── access.log 
d├── conf.d 
d├── default.d
d├── logs
d├── run
```
2. 安装docker脚本
```shell
#!/bin/bash
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
				 
sudo yum install -y yum-utils
yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
sudo yum-config-manager --enable docker-ce-nightly 
sudo yum-config-manager --enable docker-ce-test
sudo yum-config-manager --disable docker-ce-nightly
sudo yum install -y docker-ce docker-ce-cli containerd.io
mkdir /etc/docker/
touch daemon.json
cat > /etc/docker/daemon.json <<EOF
{
    "registry-mirrors": ["http://hub-mirror.c.163.com"]
}
EOF
echo "---ok---"
systemctl start docker
systemctl enable docker
```
2.mariadb redis下载安装与运行
```shell
# 安装redis
docker pull redis
# 运行redis
docker run -d --name redis-1 --rm  \
-v /var/www/redis/data:/data \
-v /var/www/redis/conf:/etc/redis/conf \
-p 6379:6379 redis:7.0.10

# 安装mariadb 
docker pull mariadb 
# 运行mariadb
# 其中MYSQL_ROOT_PASSWORD=root:设置root密码为xxx,注意:这里需要在项目配置中修改对应的数据库密码
docker run -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=xxx --name mariadb-1 \
-v /var/www/mysql/data/:/var/lib/mysql
mariadb:10.7
```




#### 使用说明

1.  (推荐)可使用nginx + uwsgi部署

#### 参与贡献
1.  @zhugxu,@龙哥,@xijin,@馨溪

#### 版本说明
1.0 初始化，有注册，登录 添加笔记，笔记列表，笔记查看功能
2.0 使用jwt校验登录功能，已经可以对接公众号项目

#### 页面截图
![输入图片说明](static/images/index.png)

1. 登录页面
![输入图片说明](web_login.png)
2. 错误提示页面
![输入图片说明](web_addnotes.png)
3. 后台管理
![输入图片说明](image.png)
![输入图片说明](admin_image.png)
4. 公众号机器人，优惠券查询，测试公众号【给点知识】
![输入图片说明](f95f5e8a-db14-4897-926f-89a052ada550.jpg)


### 需求记录：
- 需求1：通过使用django实现一个具有登录功能的网站，可以填写表单功能
- 需求2：实现cookie存储token登录使用。
- 需求3：session 实现登录对数据库读写太大，对于高并发不友好，使用jwt优化

### 致谢

感谢第三方接口，图灵对话接口
感谢gitee托管

### 写在后面的话
#### 项目初心
> 记录学习过程
> 为了讲明白一个知识点。
刚开始我不明白，为什么老师讲课我们大部分人听不懂，我现在终于明白了，  
想把一个人教会，我们就要站在他们的角度上思考问题，  
那么我每一个人跟自己身边或者同行业同领域的  
人经历都是基本上相似的。  
所有我们可以把我们遇到的困难如何解决的记录下来，  
在这之间我们遇到了什么困难，我们怎么一步步解决的。  
只有这样我们才能更好的传递知识，而不是照搬别人的复制粘贴。  
-- 更有一个网友的话让我更加顿悟，原话：  
```
我记得我大学时期做助教的时候教授说过的一句话，
“为什么有的时候，知识渊博的教授怎么也教不明白的知识，
让你们助教或者刚刚学会某些知识的小白来教会有奇效，
因为教授们已经掌握这些知识太久太久，他们忘了学习这些知识的过程，
忘了学习过程中遇到的困难，但是这些东西恰恰是刚掌握这些知识的人最清楚的。”

```
是啊只有经历过才懂得如何去解决，那么本项目就是从初始创建到一步步根据需求改变，我会记录我在这过程中遇到的问题和解决方案。
从我想做一个网站开始:从问题入手
功能一览表：
✅：已完成 ⛔:进行中 🚫:未进行
1. 如何写登录 ✅
2. 如何校验登录✅
3. 如何防止各种攻击，ddos xss等等✅
4. 如何部署 ✅
5. 如何迁移 ⛔
6. 如何实现高可用负载均衡✅
7. 如何对接第三方对接qq ✅微信公众号✅
8. 如何优化网站🚫
9. 如何实现前后端分离🚫
10. 如何处理高并发🚫
11. 如何设置缓存✅
12. 如何使用docker部署✅
13. 如何购买服务器✅
14. 如何部署集群🚫
15. 如何购买域名✅
16. 如何设置https加密✅
17. 如何备案✅
18. 如何维护，比如如何实现监控⛔
……等等。更具需求的不断堆积，我将让此项目一步步走向成熟，也将记录下来大家一起学习。


:tw-1f680:  :tw-1f680:  :tw-1f680:以上内容已经更新 :tw-1f680:  :tw-1f680:  :tw-1f680:   
======================================================================================== ==  
:tw-1f680:  :tw-1f680:  :tw-1f680:以下内容正在完善中  :tw-1f680:  :tw-1f680:  :tw-1f680:    
  





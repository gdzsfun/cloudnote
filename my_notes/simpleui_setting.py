# -*- coding: utf-8 -*-
#!/usr/bin/python
# @Date : 2022-02-14
# @Author : zhu
# @File : simpleui配置
# @Software: VS

# 禁止渲染版本号
SIMPLEUI_HOME_INFO = False
# 配置后台Logo
SIMPLEUI_LOGO = "https://kkndme.xyz/static/images/tiger.png"

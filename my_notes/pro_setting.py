# -*- coding: utf-8 -*-
#!/usr/bin/python
# @Date : 2022-02-14
# @Author : zhu
# @File : 开发环境配置
# @Software: VS

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': "notes",
        'USER':'root',
        'PASSWORD':"root",
        'HOST':'172.17.0.7',
        'PORT':'3306'
    }
}
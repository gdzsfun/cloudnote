from django.apps import AppConfig


class WxgzhConfig(AppConfig):
    name = 'wxgzh'
    verbose_name="微信公众号"

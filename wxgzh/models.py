from django.db import models

# Create your models here.

class WeChatTip(models.Model):
    key_ename=models.CharField(verbose_name='关键字英文', max_length=30,unique=True)
    key_cname=models.CharField(verbose_name='关键字中文', max_length=30,unique=True)
    content = models.TextField(null=True, blank=True, verbose_name='回复内容')
    create_time = models.DateTimeField(null=True, blank=True, verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(null=True, blank=True, verbose_name='更新时间', auto_now=True)
    remark = models.CharField(null=True,blank=True,max_length=1024, verbose_name='备注')
    def __str__(self):
        return 'key_cname %s' % self.key_cname
    class Meta:
        db_table = 'wechat_tip'
        verbose_name = '微信公众号回复'
        verbose_name_plural = verbose_name
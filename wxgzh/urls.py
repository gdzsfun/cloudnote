from django.urls import re_path
from .views import *

urlpatterns = [
    re_path('^receive/text/',WxReceiveText.as_view(),name='wx_receive_text'),
]
from django.contrib import admin
from .models import WeChatTip
# Register your models here.
class WeChatTipAdmin(admin.ModelAdmin):

    list_display = ['key_ename','key_cname','content','create_time','update_time','remark']

    def __str__(self,obj):
        return obj.key_cname

admin.site.register(WeChatTip,WeChatTipAdmin)



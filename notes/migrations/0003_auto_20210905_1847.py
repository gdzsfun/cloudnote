# Generated by Django 2.2 on 2021-09-05 10:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notes', '0002_auto_20210905_1721'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='is_pub',
            field=models.IntegerField(choices=[(0, '私有'), (1, '公开')], default=1, verbose_name='公开/私有'),
        ),
        migrations.AddField(
            model_name='note',
            name='status',
            field=models.IntegerField(choices=[(0, '删除'), (1, '正常'), (-1, '封禁')], default=1, verbose_name='状态'),
        ),
    ]

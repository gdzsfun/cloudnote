from django.urls import re_path
from .views import *

urlpatterns = [
    re_path('^add/$',note_add, name='note_add' ),                    # 笔记添加
    re_path(r'^list/',note_list, name='note_list' ),  # 笔记列表
    re_path(r'^detail/(?P<id>\d+)/$',note_detail, name='note_detail'),# 笔记详情
    re_path(r'^csv/$',note_csv, name='note_csv'),# 笔记详情
    # re_path(r'^user/$',django_user, name='note_django_user'),# 笔记详情
    re_path(r'^test_upload/$',test_upload, name='test_upload'),# 笔记详情
]
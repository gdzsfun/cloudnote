from django.apps import AppConfig


class NotesConfig(AppConfig):
    name = 'notes'
    verbose_name="点点-笔记"

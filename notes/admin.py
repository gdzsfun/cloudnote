from django.contrib import admin
from .models import *
# Register your models here.
class NoteModelAdmin(admin.ModelAdmin):
    list_display = ['id','title','likes','is_pub','collections','reading','create_time']
    list_editable = ['title','is_pub','likes','collections','reading']
class FileModelAdmin(admin.ModelAdmin):
    list_display = ['filename','file']


admin.site.register(Note, NoteModelAdmin)
admin.site.register(File, FileModelAdmin)
from ast import keyword
from pydoc import describe
from statistics import mode
from django.db import models
# from DjangoUeditor.models import UEditorField
from user.models import User
from DjangoUeditor.models import UEditorField
# Create your models here.

class NoteCategory(models.Model):
    STATUS = (
        (0,'删除'),
        (1,'正常'),
        (-1,'关闭'),
    )
    name = models.CharField(max_length=20, verbose_name='分类名称')
    status = models.IntegerField(choices=STATUS,default=1,verbose_name='状态')
    
    class Meta:
        db_table = 'note_category'
        verbose_name = '笔记分类'
        verbose_name_plural = verbose_name

class Note(models.Model):
    STATUS = (
        (0,'删除'),
        (1,'正常'),
        (-1,'封禁'),
    )
    IS_PUB = (
        (0,'公开'),
        (1,'私有'),
    )
    TYPE_STATUS = (
        (0,'原创'),
        (1,'转载'),
        (2,'翻译'),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='用户')
    category = models.CharField(max_length=100,verbose_name='文章分类',default='')
    title = models.CharField(max_length=100, verbose_name='标题')
    describe = models.CharField(max_length=200, verbose_name='描述', default='暂无', blank=True, null=True)
    keyword = models.CharField(max_length=100, verbose_name='关键字', default='暂无', blank=True, null=True)
    content = UEditorField('内容', width=800, height=500, 
                    toolbars="full", imagePath="{{MEDIA_ROOT}}/images", filePath="{{MEDIA_ROOT}}/file",
                    upload_settings={"imageMaxSize": 1204000},
                    settings={}, command=None, blank=True
                    )
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')
    status = models.IntegerField(choices=STATUS,default=1,verbose_name='状态')
    is_pub = models.IntegerField(choices=IS_PUB,default=1,verbose_name='公开/私有')
    level = models.IntegerField(default=0,verbose_name='等级')
    n_type = models.IntegerField(choices=TYPE_STATUS,default=1,verbose_name='文章类型')
    likes = models.IntegerField(default=1,verbose_name='点赞数')
    collections = models.IntegerField(default=1,verbose_name='收藏数')
    reading = models.IntegerField(default=1,verbose_name='阅读量')

    class Meta:
        db_table = 'notes'
        verbose_name = '笔记管理'
        verbose_name_plural = verbose_name


class File(models.Model):

    filename = models.CharField(max_length=12,verbose_name='文件名')
    # upload_to 表示指定的文件夹名
    file = models.FileField(verbose_name='文件',upload_to='picture')
    class Meta:
        verbose_name = '文件管理'
        verbose_name_plural = verbose_name
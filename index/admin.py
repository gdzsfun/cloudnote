from django.contrib import admin
from .models import *
# Register your models here.


# 定义网站功能开关模型
@admin.register(WebConfigModel)
class WebConfigAdmin(admin.ModelAdmin):
    list_display = ('id','function_name', 'response_content','url', 'status','notes')
    list_editable = ['status','response_content','url']
    empty_value_display = '--'


# 定义网站功能开关模型
@admin.register(VisitInfo)
class VisitInfoAdmin(admin.ModelAdmin):
    # 展示字段
    list_display = ('id','ip', 'count','url','detail', 'update_time','create_time')
    empty_value_display = '--'

# simpleui 设置标题
admin.site.site_header = '给点知识'  # 设置header
admin.site.site_title = '给点知识'   # 设置title
admin.site.index_title = '给点知识'
from django.db import models

# Create your models here.

class WebConfigModel(models.Model):
    STATUS = (
        (0,'打开'),
        (1,'关闭'),
        (-1,'暂停'),
    )
    function_name = models.CharField(max_length=100, verbose_name='功能名称')
    response_content = models.CharField(max_length=100, verbose_name='返回内容')
    status = models.IntegerField(choices=STATUS, verbose_name="功能状态", default=0)
    url = models.CharField(max_length=200,verbose_name="路由", default='')
    create_time = models.DateTimeField(verbose_name="创建时间", blank=True, null=True, auto_now_add=True)
    update_time = models.DateTimeField(verbose_name="更新时间", blank=True, null=True, auto_now=True)
    notes = models.CharField(max_length=100,verbose_name="备注信息", blank=True, null=True)

    def __str__(self):
        return self.function_name

    class Meta:
        db_table = "web_config"
        verbose_name = "路由开关" 
        verbose_name_plural= verbose_name


#访问网站的ip地址和次数
class VisitInfo(models.Model):
    ip = models.CharField(max_length=30, verbose_name='IP地址')    #ip地址
    count = models.IntegerField(verbose_name='访问次数',default=1) #该ip访问次数
    url = models.CharField(max_length=500, verbose_name="访问路由", blank=True, null=True)
    detail = models.CharField(max_length=500, verbose_name="详细消息", blank=True, null=True)
    create_time = models.DateTimeField(verbose_name="第一次访问", blank=True, null=True, auto_now_add=True)
    update_time = models.DateTimeField(verbose_name="最近一次", blank=True, null=True, auto_now=True)
    class Meta:
        verbose_name = '访问信息'
        verbose_name_plural = verbose_name
    def __str__(self):
        return self.ip







from django.apps import AppConfig


class IndexConfig(AppConfig):
    name = 'index'
    verbose_name = "点点-主页"

from django.conf.urls import url, re_path
from .views import *
urlpatterns = [
    re_path('^$', index, name='index'), # 首页
    re_path('^logout/$', logout, name='logout'), # 退出
    re_path('^varify/code/$',varify_code_view,name='varify_code'), # 获取图片验证码
    re_path('^error/forbid/$',NotFoundView.as_view(),name='forbid'), # 获取图片验证码
    re_path('^nav/index/$',NavIndex.as_view(),name='forbid'), # 导航首页页面
    re_path('^nav/common/$',NavCommon.as_view(),name='forbid'), # 导航常用页面
    re_path('^nav/develop/$',NavDevelop.as_view(),name='forbid'), # 导航develop页面
    re_path('^nav/tools/$',NavTools.as_view(),name='forbid'), # 导航tools页面
]
asgiref==3.4.1
certifi==2021.10.8
charset-normalizer==2.0.7
Django==3.0
idna==3.3
mysqlclient==2.0.3
Pillow==8.3.1
PyMySQL==1.0.2
pytz==2021.1
redis==3.5.3
requests==2.26.0
sqlparse==0.4.1
typing-extensions==3.10.0.2
urllib3==1.26.7
# uWSGI==2.0.19.1
